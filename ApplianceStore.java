import java.util.Scanner;
public class ApplianceStore {
	public static void main (String args[]){
	// create array for blender
	Blender[] bl= new Blender[4];
	Scanner reader = new Scanner(System.in);
	
	for (int i = 0; i < bl.length; i++){
	bl[i] = new Blender();	
	//get input from users
	System.out.println("What is the brand of this blender?");
	bl[i].brands = reader.next();
	
	System.out.println("What is the price(interger)of this blender?");
	bl[i].price = reader.nextInt();
	
	System.out.println("What is the color of this blender(i.e.pink,blue,yellow)?");
	bl[i].color = reader.next();	
	}
	
	//print the 3 field of the last blender in the array;
	System.out.println("The last blender's brand, price and color is:");
	System.out.println(bl[3].brands);
	System.out.println(bl[3].price);
	System.out.println(bl[3].color);
	
	
	
	System.out.println("Here is the brand of your first Blender and the smoothie you will get:");
	bl[0].printBrands();
	bl[0].smoothie();
	
	}
}